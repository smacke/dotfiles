filetype plugin on
set nocompatible
set background=dark
syntax on
set nohlsearch
set autoindent
set smartindent
set tabstop=4
set shiftwidth=4
set showmatch
set vb t_vb=
set expandtab
set incsearch
set ruler
set virtualedit=all
set backspace=indent,eol,start
set ffs=unix
set wildmode=longest,list,full
set wildmenu
"set undofile
let Tlist_Show_One_File=1
map <C-H> <C-W>h
map <C-L> <C-W>l
"map  :w!:!clear; echo Making Postscript % ...; latex %; xdvi %<.dvi&
map <C-\> :tab split<CR>:exec("tag ".expand("<cword>"))<CR>
map fj :TlistToggle<CR>
vmap // :s!^!//!<CR>
vmap dk :s!^//!!<CR>
vmap 5 :s!^!%%!<CR>
vmap 6 :s!^%%!!<CR>
imap fj <Esc>
vmap fj <Esc>
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
