#!/bin/bash

# ~/.bash_aliases: executed by .bashrc
# contains aliases (STM and functions)

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    alias ls='ls -F --color=auto'
    alias ll='ls -Al --color=auto'
    alias la='ls -AF --color=auto'
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'
    alias grep='grep --color=always' # why again? I forgot
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'

fi

# some more helpful aliases
alias cp='cp -i'
alias mv='mv -i'
#alias rm='rm -i'

alias clera='clear'
alias sr='screen -D -R'
alias c='cd ; clear'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# for opening files based on extension/type
alias 'open'='xdg-open "$@" 2>/dev/null'

# for timestamps to append to files
alias timestamp='date -u +"%Y-%m-%d-%H:%M:%S"'

# copy/paste with xclip
alias xclip='xclip -selection c'

# for sublime without borking terminal
function sublime() { sublime_text "$@" & }

function goto() {
    NAME="$1"
    PRINT="${@: -1}"
    if [ -n "$2" -a "$2" != "-print" ]; then
        HINT="$2"
    else
        HINT="."
    fi
    # need bash (not just sh)
    # if NAME contains a /, use path search
    if [[ "$NAME" == */* ]]; then
        NAME="*$NAME"
        PRED="-ipath"
        # get rid of trailing /'s (find doesn't want these)
        NAME="$(python -c "import sys; print sys.argv[1].rstrip('/')" "$NAME")"
    else
        PRED="-iname"
    fi
    DIR=$(
            # hacked-up breadth-first find
            for (( d=0; ; d++ )) do
                RESULT="$(find "$HINT" -mindepth $d -maxdepth $d "$PRED" "$NAME" -print0 -quit)"
                if [ -n "$RESULT" ]; then
                    echo "$RESULT"
                    break
                fi
            done
         )

    if [ ! -d "$DIR" -a ! "$PRINT" == "-print" ]; then
        DIR="$(dirname "$DIR")"
    fi

    if [ "$PRINT" == "-print" ]; then
        echo "$DIR"
    else
        cd "$DIR"
    fi
}
