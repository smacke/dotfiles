#!/usr/bin/env python

import os, fnmatch, datetime

# BE SURE TO REMEMBER COMMAS
DOT_FILES = [
    "bashrc",
    "bash_aliases",
    "emacs",
    "vimrc",
    "gitconfig",
    "gitignore",
    "inputrc",
    "profile",
    "vrapperrc",
    ]

IGNORE_PATTERNS = [
    "*~",
    ".git",
    "make_links.py",
    "sbclrc",
    "screenrc",
    "README",
    ]

for f in DOT_FILES:
    dot_name = os.path.join(os.environ["HOME"], "." + f)
    rel_f = os.path.join(os.path.relpath(os.curdir, os.environ["HOME"]), f)
    if os.path.lexists(dot_name):
        if os.path.islink(dot_name):
            if os.readlink(dot_name) == rel_f:
                print "already linked: %s" % f
                continue
        backup_name = dot_name + "." + datetime.datetime.now().isoformat()
        print "rename: %s -> %s" % (dot_name, backup_name)
        os.rename(dot_name, backup_name)
    print "symlink: %s -> %s" % (dot_name, rel_f)
    os.symlink(rel_f, dot_name)

for f in os.listdir(os.curdir):
    if f not in DOT_FILES:
        matched = False
        for p in IGNORE_PATTERNS:
            if fnmatch.fnmatch(f, p):
                matched = True
        if not matched:
            print "warning: %s not linked" % f
